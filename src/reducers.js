import {combineReducers} from 'redux';
import coreReducers from './modules/core/coreReducers';

const rootReducers = combineReducers({core: coreReducers});

export default rootReducers;
