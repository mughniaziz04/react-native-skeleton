import * as CONST from './coreConstant';
import * as STATE from './coreStates';

const coreInitialState = {
  ...STATE.coreInitialState,
  action: '',
};

const coreReducers = (state = coreInitialState, action) => {
  const {payload, type} = action;

  const actions = {
    [CONST.GET_DATA]: () => ({
      ...state,
      data: payload,
      action: type,
    }),
    DEFAULT: () => state,
  };
  return (actions[type] || actions.DEFAULT)();
};

export default coreReducers;
